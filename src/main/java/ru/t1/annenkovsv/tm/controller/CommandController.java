package ru.t1.annenkovsv.tm.controller;

import ru.t1.annenkovsv.tm.constant.ArgumentConst;
import ru.t1.annenkovsv.tm.constant.CommandConst;
import ru.t1.annenkovsv.tm.constant.InformationConst;
import ru.t1.annenkovsv.tm.util.MemoryCalculationUtil;
import ru.t1.annenkovsv.tm.model.Command;
import ru.t1.annenkovsv.tm.api.ICommandService;
import ru.t1.annenkovsv.tm.api.ICommandController;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        System.out.println("Current task manager commands and arguments list:");
        final Command[] commands = commandService.getCommands();
        for (final Command command: commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer name: %s\n", InformationConst.DEVELOPERNAME);
        System.out.printf("Email: %s\n", InformationConst.EMAIL);
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("Current program version: %s\n", InformationConst.CURRENTVERSION);
        System.out.printf("Last updated: %s\n", InformationConst.LASTUPDATED);
    }

    @Override
    public void showMemoryInfo() {
        System.out.println("[SYSTEM INFORMATION]");
        System.out.println("Available processors (cores): " + MemoryCalculationUtil.getAvailableProcessors());
        System.out.println("Free memory: " + MemoryCalculationUtil.getFreeMemory());
        System.out.println("Max memory: " + MemoryCalculationUtil.getMaxMemory());
        System.out.println("Total memory: " + MemoryCalculationUtil.getTotalMemory());
        System.out.println("Memory usage: " + MemoryCalculationUtil.getUsedMemory());
    }

    @Override
    public void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    @Override
    public void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("Input argument are not valid, please use arguments from list below:");
        System.err.printf("%s, %s, %s, %s\n", ArgumentConst.HELP, ArgumentConst.ABOUT, ArgumentConst.VERSION, ArgumentConst.INFO);
    }

    @Override
    public void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("Input command are not valid, please use commands from list below:");
        System.err.printf("%s, %s, %s, %s, %s\n", CommandConst.HELP, CommandConst.ABOUT, CommandConst.VERSION, CommandConst.INFO, CommandConst.EXIT);
    }

}
