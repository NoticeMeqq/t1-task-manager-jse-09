package ru.t1.annenkovsv.tm.repository;

import ru.t1.annenkovsv.tm.api.ICommandRepository;
import ru.t1.annenkovsv.tm.constant.ArgumentConst;
import ru.t1.annenkovsv.tm.constant.CommandConst;
import ru.t1.annenkovsv.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "Current task manager version."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "Information on developer."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "Program arguments list."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "Memory usage and system information."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "Exit program."
    );

    private static final Command[] COMMANDS = new Command[] {
            INFO, ABOUT, HELP, VERSION, EXIT
    };

    public Command[] getCommands(){
        return COMMANDS;
    }

}
