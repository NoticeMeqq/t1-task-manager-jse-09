package ru.t1.annenkovsv.tm.api;

public interface ICommandController {

    void showHelp();

    void showAbout();

    void showVersion();

    void showMemoryInfo();

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

}
